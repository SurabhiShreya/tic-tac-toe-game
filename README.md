Tic Tac Toe Game

This project demonstrates a Tic Tac Toe game, which is developed using HTML, CSS and JavaScript. This game can be played by two different users. Its a two player game. To understand what Tic Tac Toe game is, description is provided below.

Description:

Tic Tac Toe is a two player game. In this game, there is a 3x3 grid, upon which the game is being played. The motive of a player is to get three similar symbols in a row, horizontally, vertically, or diagonally. Each player, one by one make their respective marks in the blocks in the grid. The marks are either X or O.
